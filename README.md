# IFC-PSET Generator
- Version: 0.1
- License: GNU GENERAL PUBLIC LICENSE
- Developer: Sang I. Park

# Development Environment & Libraries
- Java 1.8.0_181
- ST-Developer for Java Lib. 1.0
- IntelliJ IDEA

# How to Run
- [Youtube](https://youtu.be/3qVMYXT0n8Y)

# Executable Jar
- [Download](https://l.linklyhq.com/l/ZKim)
