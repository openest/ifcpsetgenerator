package eu.openest.ifc.pset;

import com.steptools.schemas.ifc4.Schema;
import com.steptools.stdev.*;
import eu.openest.ifc.pset.utility.*;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainGui extends JFrame
{
    private JButton bt_ifcOpen;
    private JTextArea ta_result;
    private JPanel mainPanel;
    private JScrollPane sp_result;
    private JLabel lb_entityName;
    private JComboBox cb_entities;
    private JButton bt_getEntities;
    private JButton bt_addPSETData;
    private JButton bt_generate;
    private JLabel lb_nominalValue;
    private JLabel lb_psetDataName;
    private JLabel lb_psetName;
    private JTextField tf_openFile;
    private JTextField tf_entityName;
    private JTextField tf_psetName;
    private JTextField tf_psetDataName;
    private JTextField tf_nominalValue;
    private JButton bt_addEntity;
    private JButton bt_clear;
    private JButton bt_exit;

    private static Flags flags = new Flags();
    private static IfcInterface ifcInterface = new IfcInterface();

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                SchemaBase sb = Schema.SCHEMA;
                ifcInterface.setSchemaBase(sb);

                MainGui mainGui = new MainGui();
                mainGui.setVisible(true);
            }
        });
    }

    public MainGui()
    {
        add(mainPanel);
        setBounds(100, 100, 800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("IFC-PSET Generator V0.1 by S.I. Park");
        ta_result.setBackground(Color.lightGray);
        ta_result.setEditable(false);


        bt_ifcOpen.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                tf_openFile.setText("");
                JFileChooser fileChooser = new JFileChooser(".");
                fileChooser.setDialogTitle("Select IFC file");
                FileFilter ifcFilter = new ExtensionFileFilter("ifc ", new String[] { "ifc" });
                fileChooser.addChoosableFileFilter(ifcFilter);
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION)
                {
                    File selectedFile = fileChooser.getSelectedFile();
                    flags.setIfcFileName(selectedFile.toString());
                    flags.setIfcFilePath(fileChooser.getCurrentDirectory().toString());

                    tf_openFile.setText(flags.getIfcFileName());
                }

                // Population
                try
                {
                    ifcInterface.setPop(flags.getIfcFileName());
                    ifcInterface.setEntityIDTable();
                    lb_entityName.setVisible(true);
                    tf_entityName.setVisible(true);
                    bt_getEntities.setVisible(true);
                }
                catch (STDevException ex)
                {
                    ex.printStackTrace();
                }

            }
        });

        java.util.List<EntityInstance> eiList = new ArrayList<EntityInstance>();
        final int[] formerItemCount = {0};
        final int[] currentItemCount = {0};
        final int[] getEntitiesClickCount = {0};
        bt_getEntities.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cb_entities.removeAllItems();
                String entityName = tf_entityName.getText();
                EntityDomain ed = IfcInterface.getEntityDomain(entityName);
                if (ed != null)
                {
                    EntityExtent prods = ifcInterface.getPop().getExtent(ed);
                    EntityInstanceSetIterator itor = prods.extentIterator();
                    while (itor.hasNext())
                    {
                        EntityInstance ei = (EntityInstance) itor.next();

                        String nameAttInIFCFile = "";
                        com.steptools.stdev.Attribute[] atts = ei.getLocalDomain().getAllAttributes();
                        for (com.steptools.stdev.Attribute att : atts)
                        {
                            if (att.getName().equals("Name"))
                            {
                                nameAttInIFCFile = (String) att.getValue(ei);
                                break;
                            }
                        }
                        //ta_result.append(ei + " - " + nameAttInIFCFile + "\n");
                        eiList.add(ei);
                        cb_entities.addItem("#" + ifcInterface.getEntityIDTable().getId(ei) + ", " + ei.getLocalDomain().getName() + ", " + nameAttInIFCFile);
                    }
                    formerItemCount[0] = currentItemCount[0];
                    currentItemCount[0] = formerItemCount[0] + cb_entities.getItemCount();

                    cb_entities.setVisible(true);
                    getEntitiesClickCount[0] += 1;
                    bt_addEntity.setVisible(true);
                }
                else { tf_entityName.setText("Wrong entity name."); }

            }
        });


        java.util.List<Integer> eiItems = new ArrayList<Integer>();
        bt_addEntity.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                int selectedIdx = formerItemCount[0] + cb_entities.getSelectedIndex();
                eiItems.add(selectedIdx);

                //ta_result.append("selected index: " + selectedIdx + "\n");
                ta_result.append("Added entity: " + String.valueOf(cb_entities.getSelectedItem()) + "\n");
                lb_psetName.setVisible(true);
                tf_psetName.setVisible(true);
                lb_psetDataName.setVisible(true);
                tf_psetDataName.setVisible(true);
                lb_nominalValue.setVisible(true);
                tf_nominalValue.setVisible(true);
                bt_addPSETData.setVisible(true);
            }
        });

        final String[] psetTitle = {""};
        java.util.List<String> valueTitleList = new ArrayList<String>();
        java.util.List<String> valueDataList = new ArrayList<String>();
        bt_addPSETData.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (psetTitle[0].equals(""))
                {
                    psetTitle[0] = tf_psetName.getText();
                    ta_result.append("PSET Name: " + psetTitle[0] + "\n");
                }
                String psetDataName = tf_psetDataName.getText();
                valueTitleList.add(psetDataName);
                String nominalValue = tf_nominalValue.getText();
                valueDataList.add(nominalValue);
                tf_psetName.setEditable(false);
                tf_psetName.setBackground(Color.lightGray);

                //ta_result.setText(ta_result.getText() + "PSET Data Name: " + psetDataName + ", Nominal Value: " + nominalValue+"\n");
                ta_result.append("PSET Data Name: " + psetDataName + ", Nominal Value: " + nominalValue + "\n");
                bt_generate.setVisible(true);
            }
        });



        bt_generate.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                ifcInterface.setIfcRelDefinesByProperties(eiList, eiItems, psetTitle[0], valueTitleList, valueDataList);
                try
                {
                    ifcInterface.writeIFCfile(flags.getIfcFileName() + "_addingPSET");
                    ta_result.append("Generated: " + flags.getIfcFileName() + "_addingPSET.ifc");
                    bt_clear.setVisible(true);
                    bt_exit.setVisible(true);

                    bt_ifcOpen.setEnabled(false);
                    tf_openFile.setEditable(false);
                    tf_openFile.setBackground(Color.lightGray);
                    tf_entityName.setEditable(false);
                    tf_entityName.setBackground(Color.lightGray);
                    bt_getEntities.setEnabled(false);
                    cb_entities.setEnabled(false);
                    bt_addEntity.setEnabled(false);
                    tf_psetDataName.setEditable(false);
                    tf_psetDataName.setBackground(Color.lightGray);
                    tf_nominalValue.setEditable(false);
                    tf_nominalValue.setBackground(Color.lightGray);
                    bt_addPSETData.setEnabled(false);
                    bt_generate.setEnabled(false);
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }

            }
        });

        bt_clear.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //getContentPane().removeAll();
                //getContentPane().repaint();
                dispose();
                //System.exit(0);
                MainGui newGui = new MainGui();
                newGui.setVisible(true);
            }
        });
        bt_exit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                dispose();
                System.exit(0);
            }
        });
    }
}
