package eu.openest.ifc.pset.utility;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ExtensionFileFilter extends FileFilter
{
    String description;
    String extensions[];

    //======================================================================(S: 2)
    public ExtensionFileFilter(String description, String extension)
    {
        this(description, new String[]
                { extension });
    }

    public ExtensionFileFilter(String description, String extensions[])
    {
        // 만약 description == null 이라면, 처음 "파일이름+{갯수}"의 형식으로 표현된다.
        if (description == null)
        {
            this.description = extensions[0] + "{ " + extensions.length + "} ";
        }
        else
        {
            this.description = description;
        }

        this.extensions = (String[]) extensions.clone();
        toLower(this.extensions);
    }
    //======================================================================(E: 2)

    private void toLower(String array[])
    {
        for (int i = 0, n = array.length; i < n; i++)
        {
            array[i] = array[i].toLowerCase();
        }
    }

    //===========================================================(S: 1)
    public String getDescription()
    {
        return description;
    }

    public boolean accept(File file)
    {
        if (file.isDirectory())
        {
            return true;
        }
        else
        {
            String path = file.getAbsolutePath().toLowerCase();
            for (int i = 0, n = extensions.length; i < n; i++)
            {
                String extension = extensions[i];
                if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.'))
                {
                    return true;
                }
            }
        }
        return false;
    }
}
