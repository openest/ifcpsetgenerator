package eu.openest.ifc.pset.utility;

public class Flags
{
    private String ifcFileName;
    private String ifcFilePath;

    public String getIfcFileName()
    {
        return this.ifcFileName;
    }
    public void setIfcFileName(String ifcFileName)
    {
        this.ifcFileName = ifcFileName;
    }
    public String getIfcFilePath()
    {
        return this.ifcFilePath;
    }
    public void setIfcFilePath(String ifcFilePath)
    {
        this.ifcFilePath = ifcFilePath;
    }
}
