package eu.openest.ifc.pset.utility;

import com.steptools.schemas.ifc4.*;
import com.steptools.stdev.*;
import com.steptools.stdev.p21.EntityIDTable;
import com.steptools.stdev.p21.Part21Parser;
import com.steptools.stdev.p21.Part21Writer;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class IfcInterface
{
    private static SchemaBase _sb;
    private static Model _model;
    private static Population _pop;
    private static EntityIDTable _entityIDTable;

    public SchemaBase getSchemaBase()
    {
        return this._sb;
    }
    public void setSchemaBase(SchemaBase _sb)
    {
        this._sb = _sb;
    }

    public EntityIDTable getEntityIDTable()
    {
        return this._entityIDTable;
    }
    public void setEntityIDTable()
    {
        this._entityIDTable = EntityIDTable.forModel(this._model, true);
    }

    public Model getModel()
    {
        return this._model;
    }
    public void setModel(Model _model)
    {
        this._model = _model;
    }

    public Population getPop()
    {
        return this._pop;
    }
    public void setPop(String inputIfcFileName) throws STDevException
    {
        Part21Parser parser = new Part21Parser();
        try
        {
            this._model = parser.parse(inputIfcFileName);
            this._pop = (Population) this._model.getPopulation();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static EntityDomain getEntityDomain(String entityName)
    {
        try
        {
            EntityDomain ed = _sb.findEntityDomain(entityName);
            return ed;
        }
        catch (SchemaStructureException e)
        {
            return null;
        }
        catch (DomainNotFoundException e)
        {
            return null;
        }
    }

    public void setIfcRelDefinesByProperties(List<EntityInstance> relatedObjects, List<Integer> eiItems, String psetTitle, List<String> valueTitleList, List<String> valueDataList)
    {
        Ifcpropertyset ifcPropertySet_ = this.getPop().newIfcpropertyset();
        ifcPropertySet_.setGlobalid(this.getGUID());
        ifcPropertySet_.setOwnerhistory(this.getIfcOwnerHistoryInPop(this.getPop()));
        ifcPropertySet_.setName(psetTitle);
        SetIfcproperty setIfcProperty_ = new SetIfcproperty();
        for (int i = 0; i < valueTitleList.size(); i++)
        {
            Ifcpropertysinglevalue ifcPropertySingleValue_ = this.getPop().newIfcpropertysinglevalue();
            ifcPropertySingleValue_.setName(valueTitleList.get(i));
            Ifcvalue ifcValue_ = this.getPop().newIfcvalueIfctext(valueDataList.get(i));
            ifcPropertySingleValue_.setNominalvalue(ifcValue_);
            setIfcProperty_.add(ifcPropertySingleValue_);
        }
        ifcPropertySet_.setHasproperties(setIfcProperty_);


        Ifcreldefinesbyproperties ifcRelDefinesByProperties_ = this.getPop().newIfcreldefinesbyproperties();
        ifcRelDefinesByProperties_.setGlobalid(this.getGUID());
        ifcRelDefinesByProperties_.setOwnerhistory(this.getIfcOwnerHistoryInPop(this.getPop()));
        SetIfcobjectdefinition setIfcObjectDefinition_ = new SetIfcobjectdefinition();

        for (int shouldAddObject : eiItems)
        {
            setIfcObjectDefinition_.add(relatedObjects.get(shouldAddObject));
        }

        ifcRelDefinesByProperties_.setRelatedobjects(setIfcObjectDefinition_);
        Ifcpropertysetdefinitionselect ifcPropertySetDefinitionSelect_ = this.getPop().newIfcpropertysetdefinitionselect(ifcPropertySet_);
        ifcRelDefinesByProperties_.setRelatingpropertydefinition(ifcPropertySetDefinitionSelect_);
    }

    private String getGUID()
    {
        UUID uuid = UUID.randomUUID();
        byte[] uuidArr = this.asByteArray(uuid);
        String s24 = new sun.misc.BASE64Encoder().encode(uuidArr);
        String s22 = s24.split("=")[0];  // s24는 맨마지막에 == 이 포함되어 있음.
        return s22;
    }
    private byte[] asByteArray(UUID uuid)
    {
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        byte[] buffer = new byte[16];
        for (int i = 0; i < 8; i++)
        {
            buffer[i] = (byte) (msb >>> 8 * (7 - i));
        }
        for (int i = 8; i < 16; i++)
        {
            buffer[i] = (byte) (lsb >>> 8 * (7 - i));
        }
        return buffer;
    }

    private Ifcownerhistory getIfcOwnerHistoryInPop(Population pop)
    {
        EntityExtent prods = pop.getExtent(Ifcownerhistory.DOMAIN);
        java.util.Iterator itor = prods.iterator();
        while (itor.hasNext())
        {
            Ifcownerhistory ifcOwnerHistory_ = (Ifcownerhistory)itor.next();
            if (ifcOwnerHistory_ != null)
            {
                return ifcOwnerHistory_;
            }
        }
        return null;
    }

    public void writeIFCfile(String ifcFileName) throws IOException
    {
        Part21Writer writer = new Part21Writer();
        writer.write(ifcFileName+".ifc", this.getModel());
    }
}
